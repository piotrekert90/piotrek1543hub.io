import "../css/main.sass";

$("body").attr({
    "id": "home",
    "class": "page"
});

$(".nav__item").on("click", function () {
    $(".nav").find(".active").removeClass("active");
    $(this).parent().addClass("active");
});

$(function () {
    smoothScroll(300);
});

$(".subheader__title").fitText(2, {minFontSize: '12px', maxFontSize: '32px'});


$(function () {
    $('.contact__body__mail').on('click', function (event) {
        event.preventDefault();
        var email = 'piotrekert90@gmail.com';
        var subject = 'New cooperation offer';
        var emailBody = 'Dear Piotr,\n I\'m contacting you to';
        window.location = 'mailto:' + email + '?subject=' + subject + '&body=' + emailBody;
    });
});

// smoothScroll function is applied from the document ready function
function smoothScroll(duration) {
    $('a[href^="#"]').on("click", function (event) {
        var target = $($(this).attr("href"));
        event.preventDefault();
        $("html, body").animate(
            {
                scrollTop: target.offset().top
            },
            duration
        );
    });
}
